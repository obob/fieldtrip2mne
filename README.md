# fieldtrip2mne

# fieldtrip2mne is deprecated!

fieldtrip2mne has been integrated into mne python version 0.17 and
above. Future development will this take place directly in the codebase
of mne python.

Please take a look at the respective [Documentation](https://mne-tools.github.io/stable/manual/io.html#importing-meg-eeg-data-from-fieldtrip) for further details.

## License
This module is developed by Thomas Hartmann & Dirk Gütlin at the Universität Salzburg. You are free to use, copy, modify, distribute it under the terms of the BSD 2 clause license.