# -*- coding: UTF-8 -*-
# Copyright (c) 2018, Thomas Hartmann & Dirk Gütlin
# All rights reserved.
#
# This file is part of the fieldtrip2mne Project, see: https://gitlab.com/obob/fieldtrip2mne
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# * Redistributions of source code must retain the above copyright notice, this
#   list of conditions and the following disclaimer.
#
# * Redistributions in binary form must reproduce the above copyright notice,
#   this list of conditions and the following disclaimer in the documentation
#   and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import mne
import numpy
import os.path

import fieldtrip2mne

test_data_folder = 'tests/test_data'

def test_whole_process():
    all_versions = ['v7', 'v73']
    for version in all_versions:
        f_name_raw = os.path.join(test_data_folder, 'raw_%s.mat' % (version, ))
        f_name_epoched = os.path.join(test_data_folder, 'epoched_%s.mat' % (version,))
        f_name_avg = os.path.join(test_data_folder, 'averaged_%s.mat' % (version,))
        f_name_events = os.path.join(test_data_folder, 'events.eve')

        # load everything
        data_raw = fieldtrip2mne.read_raw(f_name_raw, data_name='data')
        data_epoched = fieldtrip2mne.read_epoched(f_name_epoched, data_name='data_epoched')
        data_avg = fieldtrip2mne.read_avg(f_name_avg, data_name='data_avg')
        events = mne.read_events(f_name_events)

        mne_epoched = mne.Epochs(data_raw, events, tmin=-0.2, tmax=0.2, preload=True, baseline=None)
        numpy.testing.assert_almost_equal(data_epoched.get_data(), mne_epoched.get_data()[:, :, :-1])

        mne_avg = mne_epoched.average()
        numpy.testing.assert_almost_equal(data_avg.data, mne_avg.data[:, :-1])