.. fieldtrip2mne documentation master file, created by
   sphinx-quickstart on Mon Mar 26 16:49:50 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to fieldtrip2mne's documentation!
============================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

fieldtrip2mne is deprecated!
============================

fieldtrip2mne has been integrated into mne python version 0.17 and
above. Future development will this take place directly in the codebase
of mne python.

Please take a look at the respective `Documentation <https://mne-tools.github.io/stable/manual/io.html#importing-meg-eeg-data-from-fieldtrip>`_ for further details.


